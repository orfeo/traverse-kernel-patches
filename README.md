# Traverse Kernel Patches

This repository contains patches against the mainline (kernel.org) tree for various kernel versions.

The CI scripts will generate packages for various distribution types (Debian based, RPM based), as well
as a self-contained repository that can be used by the image builder script downstream.  Periodically
the packages will be signed and uploaded to the 'master' package repository.

Note that mainline kernel 5.0 and later has all the drivers to work on DPAA2 (e.g LS1088) hardware, so your distribution
kernel might work just fine - see [our documentation](https://ten64doc.traverse.com.au/software/distributions/).

We provide this kernel to facilitate feature development and testing for patches/features that are not yet in mainline or
require backports - we anticipate (for the LS1088) this will be needed through 2020 at least.

## Current recommended branches
* lts-5-4: 5.4.x Series (Long Term Stable kernel)
* torvalds: builds against the torvalds tree ('mainline' on kernel.org)

Occasionally other branches might appear, following kernel.org conventions (stable-53) or
vendor based such as 'lsdk' (NXP's patchset).

## Debian/Ubuntu/APT packages
An experimental Debian repo is hosted at ```https://archive.traverse.com.au/pub/traverse/debian-experimental/```.
All builds here come via the [CI process](https://gitlab.com/traversetech/traverse-kernel-patches/pipelines) and are built on public cloud runners.

At the moment, this repo is *unsigned* - once our processes are stable we will create a signed repo at `/debian`.

To use them, put this in your apt-sources list:
```
deb [trusted=yes] https://archive.traverse.com.au/pub/traverse/debian-experimental/ torvalds main
```
Change `torvalds` to your desired branch above.

To install the kernel from this repo, do ``apt-get install linux-image-traverse``, which will install both the kernel and the out-of-tree/staging module package.

## Usage
```download-apply.sh``` will download a repository from kernel.org and apply the patchset described
in [patches/series](patches/series).

For CI/non-development workspaces where full history is not required, use ```GIT_ARGS="--depth 1" ./download-apply.sh```
to download a shallow tree.

CI builds are driven by Gitlab CI, see [.gitlab-ci.yml](.gitlab-ci.yml) for details. A native ARMv8 host is required for build.
