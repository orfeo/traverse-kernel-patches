#!/bin/sh
REPO_DIR=output/repo
mkdir -p output/repo/conf
SANITIZED_CODENAME=$(echo "${CI_COMMIT_REF_NAME}" | sed "s/_/-/g")

cat <<EOF > "${REPO_DIR}"/conf/distributions
Origin: traverse-kernel
Label: traverse-kernel-build
Codename: ${SANITIZED_CODENAME}
Architectures: source arm64
Components: main
Description: Traverse Kernel packages
EOF

#cp "dists/debian/_repoconf" "${REPO_DIR}"/conf/distributions
