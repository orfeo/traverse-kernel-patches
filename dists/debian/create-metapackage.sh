#!/bin/sh
set -e
SANITIZED_CODENAME=$(echo "${CI_COMMIT_REF_NAME}" | sed "s/_/-/g")

# Capture the kernel version and generate an equivs
KERNEL_PACKAGE=$(grep Binary output/kernel/*.dsc | awk -F "," '{{print $1}}' | sed -s s/"Binary: "//g)
KERNEL_VERSION=$(grep Version output/kernel/*.dsc | awk -F ': ' '{{print $2}}')

MODULES_PACKAGE=$(grep Binary output/dkms/traverse-sensors-dkms-bin*.changes | awk -F "," '{{print $1}}' | sed -s s/"Binary: "//g)

cp -r /usr/share/equivs/template .
echo "1.0" > template/debian/source/format
cat <<EOF > linux-image-traverse.control
Section: kernel
Priority: optional
Homepage: https://gitlab.com/traversetech/traverse-kernel-patches/
Standards-Version: 3.9.2

Package: linux-image-traverse
Version: ${KERNEL_VERSION}
Suite: ${SANITIZED_CODENAME}
Maintainer: Mathew McBride <matt@traverse.com.au>
Depends: ${KERNEL_PACKAGE}, ${MODULES_PACKAGE}
Architecture: arm64
Description: Metapackage for kernel with Layerscape architecture packages
EOF

# This will fail with a signing error - ignore for this purpose
equivs-build --template template -f linux-image-traverse.control || :
mkdir -p output/meta
mv linux-image-traverse*.deb output/meta
mv linux-image-traverse*.changes output/meta
mv linux-image-traverse*.dsc output/meta
mv linux-image-traverse*.buildinfo output/meta
mv linux-image-traverse*.tar.gz output/meta
