#!/bin/sh
# Traverse Kernel Build Tool
# Author: Mathew McBride <matt@traverse.com.au>
set -e

THIS_DIR=$(pwd)
MACHINE=$(uname -m)
CROSS_COMPILE=${CROSS_COMPILE:-}
GIT_ARGS=${GIT_ARGS:-}
GIT_BRANCH=${GIT_BRANCH:-master}

KERNEL_GIT=${KERNEL_GIT:-https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git}

git clone --branch "${GIT_BRANCH}" $GIT_ARGS $KERNEL_GIT linux
cp kernel-config linux/.config
cd linux
git checkout "${GIT_BRANCH}"
git checkout -b build-$(date +%s)
for i in $(cat "${THIS_DIR}/patches/series") ; do
	git am "${THIS_DIR}/patches/${i}"
done

make oldconfig ARCH=arm64
